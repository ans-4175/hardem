var express = require('express'),
	url = require('url'),
	MongoClient = require('mongodb').MongoClient,
	assert = require('assert');
var port = 3000;
var app = express();
var server = require('http').createServer(app).listen(port);
//var io = require('socket.io').listen(server);
var plantClients = [];
var dashClients = [];

//app express
app.set('views', __dirname + '/tpl');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.use(express.static(__dirname + '/public'));
//sendraw
app.get("/send/:uid", function(req, res){
	var murl = 'mongodb://localhost:27017/hardem';
	MongoClient.connect(murl, function(err, db) {
		//parse js
		var url_parts = url.parse(req.url, true);
		var query = url_parts.query;
		var data = query.data || "{}";
		var uid = req.param("uid") || "";
		var objData = JSON.parse(data);
		//prepare dataraw
		var state = (typeof objData.state !== 'undefined') ? objData.state : null;
		var rawObj = objData;
		if (typeof rawObj.state !== 'undefined') delete rawObj.state;
		rawObj.uid = uid;
		rawObj.ts = Date.now();
		//get timestamp
		if (rawObj !== null){
			var dataraw = db.collection('dataraw');
			dataraw.insert(rawObj);
			//console.log(rawObj);
		}
		//update state
		if (state !== null) {
			var tanaman = db.collection('tanaman');
			tanaman.update({uid : uid},{ $set: { state : state, status:rawObj } }, function(err, result) {
				console.log("Updated the state="+uid);
			});
		}
		//db.close();
	});
	
	res.sendStatus(200);
});
//present-on/off
app.get("/present/:on/:uid", function(req, res){
	var uid = req.param("uid") || "";
	var isOn = req.param("on") || "off";
	isOn = (isOn == 'on') ? 1 : 0;
	var murl = 'mongodb://localhost:27017/hardem';
	var ts = Date.now();
	MongoClient.connect(murl, function(err, db) {
		var tanaman = db.collection('tanaman');
		tanaman.update({uid : uid},{ $set: {Present :{ts:ts,isOn:isOn}}}, function(err, result) {
			console.log("Updated the present="+isOn);
		});
	});
	
	res.sendStatus(200);
});
//switch-device:pompa,lampu
app.get("/switch/:device/:uid", function(req, res){
	var device = req.param("device") || "pompa";
	var uid = req.param("uid") || "";
	var ts = Date.now();
	var murl = 'mongodb://localhost:27017/hardem';
	MongoClient.connect(murl, function(err, db) {
		//get db
		var tanaman = db.collection('tanaman');
		if (device == 'pompa'){
			tanaman.update({uid : uid},{ $set: { Pompa : {ts:ts,isOn:1} } }, function(err, result) {
				console.log("pompa on");
			});
		}else if (device == 'lampu'){
			tanaman.findOne({uid:uid}, function(err, item) {
				var swOn = (item.Lampu.isOn) ? 0 : 1;
				tanaman.update({uid : uid},{ $set: { Lampu : {ts:ts,isOn:swOn} } }, function(err, result) {
					console.log("lampu="+swOn);
				});
			});
		}
	});
	
	res.sendStatus(200);
});
//check-device:pompa,lampu
app.get("/check/:uid", function(req, res){
	var uid = req.param("uid") || "";
	var murl = 'mongodb://localhost:27017/hardem';
	MongoClient.connect(murl, function(err, db) {
		//get db
		var tanaman = db.collection('tanaman');
		tanaman.findOne({uid:uid}, function(err, item) {
			//increment-siram
			if (item.Pompa.isOn == 1){
				var ts = Date.now();
				tanaman.update({uid : uid},{ $inc: { countSiram:1 }, $set: { Pompa : {ts:ts,isOn:0} } }, function(err, result) {
					console.log("countSiram="+(item.countSiram+1));
				});
			}
			//kirim
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify(item));
		});
	});
});
//get
app.get("/get/:uid", function(req, res){
	var uid = req.param("uid") || "";
	var murl = 'mongodb://localhost:27017/hardem';
	MongoClient.connect(murl, function(err, db) {
		//get db
		var tanaman = db.collection('tanaman');
		tanaman.findOne({uid:uid}, function(err, item) {
			//kirim
			res.setHeader('Content-Type', 'application/json');
			res.end(JSON.stringify(item));
		});
	});
});
//dashboard
app.get("/dashboard/:uid", function(req, res){
	var uid = req.param("uid") || "";
	var murl = 'mongodb://localhost:27017/hardem';
	MongoClient.connect(murl, function(err, db) {
		//get db
		var tanaman = db.collection('tanaman');
		var dataraw = db.collection('dataraw');
		var data = {};
		tanaman.findOne({uid:uid}, function(err, item, db) {
			var d = new Date(Number(item.waktutanam));
			item.tglTanam = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
			res.render("dash",{data:item});
			//res.sendStatus(200);
		});
	});
});

/*set('log level', 0);
io.sockets.on('connection', function(client){
	//koneksi
	var idclient = client.id;
	//console.log("another connected device");
	    
    client.on('disconnect', function() {
		var index = plantClients.indexOf(client);
		if (index != -1) {
			plantClients.splice(index, 1);
		}
		var index1 = dashClients.indexOf(client);
		if (index1 != -1) {
			dashClients.splice(index1, 1);
		}
    });
});*/

console.log("Listening on port " + port);
