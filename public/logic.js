function updateui(){
	$.get(setupsocket+"/get/sikaktus", function(json) {
		//state
		var src = "";
		var state = Number(json.state);
		switch(state){
			case 0 : src="/images/sad_kaktus.png";
				break;
			case 1 : src="/images/neutral_kaktus.png";
				break;
			case 2 : src="/images/happy_kaktus.png";
				break;
			case 3 : src="/images/sleep_kaktus.png";
				break;
		}
		document.getElementById("emosi").src=src;
		//mood
		$("#humidProg").css( "width", json.status.humid+"%" );
		$("#tempProg").css( "width", json.status.temp+"%" );
		$("#luxProg").css( "width", json.status.lux+"%" );
		//raw
		document.getElementById("humidRaw").innerHTML=json.status.humidRaw;
		document.getElementById("tempRaw").innerHTML=json.status.tempRaw;
		document.getElementById("luxRaw").innerHTML=json.status.luxRaw;
		//lampu
		if (Number(json.Lampu.isOn))
			document.getElementById("isLampu").innerHTML="Lampu nyala";
		else
			document.getElementById("isLampu").innerHTML="Lampu mati";
		//orang
		if (Number(json.Present.isOn))
			document.getElementById("isPresent").innerHTML="Ada orang";
		else
			document.getElementById("isPresent").innerHTML="Tidak ada orang";
	});
}

window.onload = function() { 
	/*var socket;
	socket = io.connect(setupsocket);
	socket.on('randOut', function (data) {
		var data = JSON.parse(data);
		document.getElementById('clientRand').innerHTML = "We get a winner '"+data.id+"' and '"+data.id2+"'";
	});*/
	
	var btnSiram = document.getElementById('btnSiram');
	btnSiram.onclick = function(){
		var jqxhr = $.get(setupsocket+"/switch/pompa/sikaktus", function() {
				console.log("on siram");
			})
			.done(function() {
				updateui();
			});
	};
	
	var btnLampu = document.getElementById('btnLampu');
	btnLampu.onclick = function(){
		var jqxhr = $.get(setupsocket+"/switch/lampu/sikaktus", function() {
				console.log("switch lampu");
			})
			.done(function() {
				updateui();
			});
	};
	
	setInterval(function(){
		updateui();
	}, 1500);
}
